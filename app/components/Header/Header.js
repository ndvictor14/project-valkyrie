import './header.scss';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link,IndexLink } from 'react-router';
import { Row, Container } from '../Layouts';


class Header extends Component {

    constructor(props){
        super(props);

        this.state = {
            showSearch: false
        };
    }
    componentDidMount(){

    }

    showSearchBar(){
        this.setState({showSearch: !this.state.showSearch});
    }

    getSiderBarWidth(){
        const searchBar = ReactDOM.findDOMNode(this.refs.searchToggler);
        const demensions = searchBar.getBoundingClientRect();

        return demensions.width;
    }

    render() {

        return (
            <header className="background-color-1">
                <div className="left-links">
                    <IndexLink to="/" activeClassName='active'><i className="mi mi-home mi-24"></i></IndexLink>
                    <Link to="/test" activeClassName='active'><i className="mi mi-info-outline mi-24"></i></Link>
                </div>
                <div className="mid-links">
                </div>
                <div className="right-links">
                    <div onClick={this.showSearchBar.bind(this)}
                         ref="searchToggler"
                         className={
                             this.state.showSearch ? "search-button active" : "search-button"
                         }>
                        <i className="mi mi-search mi-24"></i>
                        {
                            this.state.showSearch ?
                                <div className="search-bar-container" ref="search_bar_container">
                                    <input type="text" className="search-bar"/>
                                </div> : ''
                        }
                    </div>
                </div>
            </header>
        );
    }
}


export default Header;
