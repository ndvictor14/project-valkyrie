import React, { Component } from 'react';
import classnames from 'classnames';



class Container extends Component {

    render() {

        return (
            <div className={classnames('container', this.props.className)}>
                {this.props.children}
            </div>
        );
    }
}


export default Container;
