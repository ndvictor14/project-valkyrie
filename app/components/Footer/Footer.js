import './footer.scss';
import React, { Component } from 'react';
import { Row, Container } from '../Layouts';


class Footer extends Component {


    render() {

        return (
            <Row className="flex-1">
                <Container>
                    Footer
                </Container>
            </Row>
        );
    }
}

export default Footer;
