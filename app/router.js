import React from 'react'
import { Route, IndexRoute } from 'react-router'

import Index from './containers/Index/Index'
import App from './components/App/App'
import PageNotFound from './components/PageNotFound/PageNotFound'
module.exports = (

    <Route path="/" component={App}>

        <IndexRoute component={Index} />
        <Route path="test" component={Index} />




        {/* Catch all*/}
        <Route path='*' component={PageNotFound} />
    </Route>

);
