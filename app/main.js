import 'normalize.css/normalize.css';
import './styles/main.scss';

import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from './router.js';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import rootReducer from './reducers';


const createStoreWithMiddleware = applyMiddleware()(createStore);

render(
    <Provider store={createStoreWithMiddleware(rootReducer)}>
        <Router routes={routes} history={browserHistory}/>
    </Provider>
    , document.getElementById('js-main')
);
