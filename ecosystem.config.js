module.exports = {
    // Run as Root
    // pm2 kill
    // pm2 startOrRestart ecosystem.config.js --env production
    // pm2 save
    // pm2 startup
    apps : [

        {
            name      : "Table-Top",
            script    : "server.js",
            env_production : {
                NODE_ENV: "production"
            },
            watch : ["./public/build"]
        },

    ],
};
